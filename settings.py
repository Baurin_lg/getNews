##############################################
###### Extractor de contenido WEB ############
###### Autor : Aerin Sistemas     ############
##############################################

# URL de la que extraer contenido
# A veces es necesario una pagina anterior de la que 
# extraer la variable de sesion. Si es el caso, indicala
# como url_sesion. Si no es el caso, escribe el mismo valor 
# en ambas.
url = "http://blog.keytron.com"
url_sesion = "http://blog.keytron.com"

# Modelado del contenido 
# Para extraer contenido es necesario crear el 
# modelo del contenido a extraer. Crear una variable 
# como las que siguen a continuacion rellenando cada 
# atributo con lo que corresponda en la web en cuestion
# La variable a de empezar por content{id__string_contenido}

contentImage = {
  
	"tag"   : "a",
	"class" : "",
	"id"    : "",
	"attrib": {},
	"finder" : ".//*[@class='rt-img-holder']/a/img",
	"get"   : "src",
	"type"  : "Imagen",
	"unique_id" : "news_image",
}

contentHeader = {

	"tag"   : "a",
	"class" : "",
	"id"    : "",
	"attrib": {},
	"get"   : "text",
	"type"  : "Imagen",
	"finder" : "class",
	"unique_id" : "news_header",
}

contentTitle = {

	"tag"   : "a",
	"class" : "",
	"id"    : "",
	"attrib": {},
	"finder" : "class",
	"get"   : "text_content",
	"type"  : "Imagen",
	"finder" : ".//*[@class='rt-detail']/h2",
	"unique_id" : "news_title",
}

contentText = {

	"tag"   : "a",
	"class" : "",
	"id"    : "",
	"attrib": {},
	"get"   : "text_content",
	"type"  : "text",
	"finder" : ".//a/..[@class='post-content']",
	"unique_id" : "news_text",
}

contentLinkMore = {

	"tag"   : "a",
	"class" : "",
	"id"    : "",
	"attrib": {},
	"get"   : "href",
	"type"  : "link",
	"finder" : ".//*[@class='post-content']/a",
	"unique_id" : "news_link_more",
}

# Una funcionalidad que finalmente no se ha usado 
# es la redireccion. Cuando buscamos el contenido enlazado
# que no aparece directamente en la web, tendremos que usar
# un objeto de este tipo. Notese el atributo type y finder_after.
# este segundo atributo sera el patron de busqueda en la pagina
# enlazada.

# contentFullArticle = {

# 	"tag"   : "",
# 	"class" : "",
# 	"id"    : "",
# 	"attrib": {},
# 	"get"   : "text_content",
# 	"type"  : "redirection",
# 	"finder" : ".//*[@class='post-content']/a",
# 	"finder_after" : ".//*[@class='entry-content']/div",
# 	"unique_id" : "news_full_text",
# }


# Configuracion de la base de datos MySQL
# No hay de momento compatibilidad para otras BBDD

password = "*********"
host     = "hostname"
db       = "DatabaseName"
user     = "user"

# Para hacer replace en un template tenemos que indicar algunos 
# parametros para poder encontrar la seccion a reemplazar y el path 
# donde se encuentre el template. tag_init y tag_end deben ser arrays
# de strings donde cada elemento representa el tag de inicio o final
# de cada nueva entrada que delimita el contenido en el template.

path_template = "./template.html"

tag_init = ["<!-- Post Item 1-->","<!-- Post Item 2-->","<!-- Post Item 3-->"]
tag_end = ["<!-- End Post Item 1-->","<!-- End Post Item 2-->","<!-- End Post Item 3-->"]
