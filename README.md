El repositorio incluye : 
 - script de ejemplo (sample.py)
 - archivo de configuración (settings.py)
 - libreria con la clase getNews (getNews.py)
 
 Esta app nos ayudara a extraer contenido de una web. Ya sea texto, enlaces, imagenes ...
 Haciendo uso de :
 - lxml (Herramienta de parseo de xml basada en libxml2 hecha c++)
 - re (modulo que implementa expresiones regulares en python)
 - requests (modulo python para gestionar variables de sesion y realizar peticiones http)
 
 Deberemos configurar la app para su correcto funcionamiento. Para ello setearemos a conveniencia
 los atributos del settings.py: 
 - Deberemos crear los elementos conten{id} tal y como se indica en el codigo comentado: comenzando el nombre
 de la variable con "content".Cada variable aludira a un contenido de la web que deseemos extraer. 
 - Tendremos que definir el patron debusqueda(finder) para referirnos a ese contenido con lxml. Para ello es 
 posible que desees consultar el manual de lxml para python (https://docs.python.org/2/library/xml.etree.elementtree.html). 
 - En caso de que el contenido que deseemos no este en la web inicial si no en una web enlazada desde un link
 de la web inicial, tendremos que definir un segundo patron de busqueda (finder_after) que aluda al contenido
 de la segunda web y el primer patron (finder) lo usaremos para encontrar el link que contenga el enlace de la web.
 También tendremos que setear "type" como "redirection".
 - type="text" # cuando el contenido sea textual
 - type="redirection"  # cuando el contenido este en una segunda web 
 - get = {"href","src","class" ... } #cualquier atributo del tag
 -tag_init/tag_end : Array de etiquetas que delimiten el contenido que se desee cambiar en el template al que apunta
 el path definido en el settings.
 - url : url de la que extraer contenido 
 - url_sesion : url de la que extraer la variable de session. A veces es necesario una pagina anterior de la que 
 extraer la variable de sesion. Si es el caso, indicala como url_sesion. Si no es el caso, escribe el mismo valor en ambas.


Para usar la clase getNews : 

#Importo la clase
>>> from getNews import *

#Instancio la clase
>>> news = getNews()

#Cojo el contenido siguiendo el archivo settings explicado al comienzo de este archivo 
>>> output = news.getOutput()

>>> for i in output.items():
>>> 	print i[0]

news_title
news_text
news_image
news_link_more

Cada uno de los json Content{id} creara un item en la variable retornada, formateando el id del 
contenido a snake case y comenzando por 'news'.

Cada uno de los items guarda un array con todas las coincidencias que se encontraran para el xpath 
descrito en el settings. 

>>> print output["news_title"][0]
Keytron de visita por cortesia de TechData al Customer Invation Center de HPE en Ginebra.
>>> print output["news_title"][1]
Keytron participa en el Aruba AGORA-TECH 2018

De modo que luego se pueda recorrer y reemplazar en el texto de destino. 

En sample.py se puede ver un ejemplo de uso para keytron. Las funciones para reemplazar se dejan en el sample 
para separar la clase para hacer el scraping de la funcion final que se le de al contenido extraido. 

En la funcion replaceContent se utiliza una expresion regular muy simple que comienza y acaba con los tags_init y tags_end
para encontrar el bloque de texto que se debe sustituir. 

En la funcion constructNewPost se construye el texto por el que se sustituira el viejo, de modo que si se trata de un html, se pueden aplicar clases, estilo o funciones javascript. 

Desde sample.py se importa la clase para poder instanciarla y usarla para extraer contenido. El settings se debe configurar previamente.

Por ultimo, para automatizar la tarea, se debe incluir en el cron de la maquina y dar permisos a la carpeta. Si la maquina usa virtualenv, se deberia importar el path para que pueda usar las librerias del virtualenv o, otra forma mas sencilla puede ser creando un sh donde se abra la carpeta del virtualenv y se active, algo asi como lo que sigue. 


cd /path/al/virtualenv
source nombrevirtualenv/bin/activate
cd /path/getNews/
python sample.py