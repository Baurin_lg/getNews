##############################################
###### Extractor de contenido WEB ############
###### Autor : Aerin Sistemas     ############
##############################################


from getNews import *

def constructNewPost(tag_init,tag_end,title,imagen,text,links_more):
    text = """
		%s
		<div class="col-sm-6 col-md-4 col-lg-4 mb-md-50 wow fadeIn" data-wow-delay="1.4s" data-wow-duration="1s">
		        
		        <div class="post-prev-img">
		            <a href=""><img src="%s" alt="Keytron S.A." /></a>
		        </div>
		        
		        <div class="post-prev-title font-alt">
		            <a href="">%s</a>
		        </div>
		        
		        <div class="post-prev-info font-alt">
		            <a href=""></a>
		        </div>
		        
		        <div class="post-prev-text text-justify">
		        %s
		        </div>
		        
		        <div class="post-prev-more">
		            <a href="%s" class="font-raleway-down btn btn-mod btn-border-azul btn-medium btn-round">Seguir leyendo</a> 
		        </div>
		        
		</div>
		%s"""%(tag_init,imagen,title,text,links_more,tag_end)

    return text


def replaceContent(path,index,output):
        
    with open(path,"r") as template:
        lines = template.readlines()
    # print output
    texto = re.sub("[a-zA-Z]* Seguir leyendo", "", output["news_text"][index]).encode("utf-8")
    imagen = output["news_image"][index].encode("utf-8")
    cabecera = output["news_title"][index].encode("utf-8")
    links_more = output["news_link_more"][index].encode("utf-8")
    
    # print "Imagen   : %s"%(imagen)
    # print "Cabecera : %s"%(cabecera)
    # print "Texto    : \n%s"%(text)
    # print settings.tag_end[index]
    
    # Patron de busqueda
    
    text_pattern = news.settings.tag_init[index]+".*"+news.settings.tag_end[index]
    newtext = constructNewPost(news.settings.tag_init[index],news.settings.tag_end[index],cabecera,imagen,texto,links_more)
    
    with open(path,"w") as template:
        old_text = True
        text_inserted = False
        for line in lines:
            
            if line.find(news.settings.tag_end[index])!= -1: 
                old_text = True
                continue
            
            if line.find(news.settings.tag_init[index])!= -1: 
                old_text = False
            
            if old_text : template.write(line)

            if not old_text and not text_inserted: 
                template.write(newtext)
                text_inserted = True

            
            if len(line)>0 and line[-1] != "\n": template.write("\n") 


if __name__ == "__main__":
    news = getNews()
    output = news.getOutput()
    path = news.settings.path_template
    for index in range(0,3):
        replaceContent(path,index,output)
    
  