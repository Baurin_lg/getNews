##############################################
###### Extractor de contenido WEB ############
###### Autor : Aerin Sistemas     ############
##############################################


from lxml import html 
import urllib2 as req
import requests
import sys
import re
import MySQLdb
import settings

        



class getNews:
    def __init__(self):
        self.settings = settings
        self.extractedContent = []
        self.contentSettings = []
        for k, v in settings.__dict__.iteritems():
            if isinstance(v, dict) and k.startswith('content'):
                self.contentSettings.append({k : v})
		        # print "%s=>%s"%(k,v)
        
        # print self.contentSettings
        self.getSessionPage()
        self.getHtml()
        self.parsingComponents(self.text)

    def connectDataBase(self):
        db = MySQLdb.connect(host=settings.host,    
                     user=settings.user,         
                     passwd=settings.password,  
                     db=settings.db)


    def getHtml(self):
        response = self.session.get(settings.url)
        self.text = response.text
		

    def parsingComponents(self,text=None):
        self.extractedContent = []
        self.tree = html.fromstring(text.encode("utf-8"))
        for content in self.contentSettings:
            self.extractedContent.append({
            	"node":self.tree.findall(content.items()[0][1]["finder"]),
            	"get" :content.items()[0][1]["get"],
            	"type" :content.items()[0][1]["type"],
            	"tag" :content.items()[0][1]["tag"],
                "attrib" :content.items()[0][1]["attrib"],
                "unique_id" :content.items()[0][1]["unique_id"],

            	})

        # print self.extractedContent

    def getSessionPage(self):
        self.session = requests.session()
        self.session.get(settings.url_sesion)

    def extractRedirectContent(self,second_url,finder):
    	response = self.session.get(second_url)
    	text = response.text
    	tree = html.fromstring(text.encode("utf-8"))
    	elements = tree.findall(finder)
    	content = []
    	for e in elements : content.append(e.text_content())
    	# print content
    	return content

    def getOutput(self):
        self.output = {}
        for k,i in enumerate(self.extractedContent):
            if len(i["node"])>0:
                extracted = []
                for content in i["node"]:
                    
                    try: 
                        if i["get"] == "text_content":
                            
                            if i["type"] == "redirection":
                                print "Consultando Contenido Externo ..."
                                second_url = content.attrib["href"]
                                extracted.append(self.extractRedirectContent(second_url,self.contentSettings[k].items()[0][1]["finder_after"]))
                            else : 
                                extracted.append(content.text_content())


                        else:
                            if i["type"] == "redirection":
                                second_url = content.attrib[content.attrib["href"]]
                                extracted.append(self.extractRedirectContent(second_url,self.contentSettings[k].items()[0][1]["finder_after"]))
                            else:
                                extracted.append(content.attrib[i["get"]])

                        # print content.attrib[i["get"]] 
                    except Exception as e: 
                        print str(e)

                self.output[i["unique_id"]] = extracted

        return self.output


